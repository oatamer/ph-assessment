package com.powerhouse.energy.test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.powerhouse.energy.Application;
import com.powerhouse.energy.dto.ProfilesDTO;
import com.powerhouse.energy.dto.ProfilesDataDTO;
import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.service.IProfileService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProfileRestTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private IProfileService<ProfilesDTO> profileService;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void profileServiceTest() throws Exception {
		List<ProfilesDataDTO> profiles = profileService.listProfilesData("dummy");
		assertThat(profiles, new IsEmptyCollection<ProfilesDataDTO>());
	}

	@Test(expected = DataException.class)
	public void updateProfileTest() throws Exception {
		profileService.updateProfile("dummy", "dummy");
	}

	@Test(expected = DataException.class)
	public void updateProfilesData() throws Exception {
		List<ProfilesDataDTO> list = new ArrayList<>();
		list.add(new ProfilesDataDTO(Months.APR, "dummy", BigDecimal.ZERO));
		profileService.updateProfilesData(list);
	}

	@Test
	public void loadProfilesFail() throws Exception {
		mockMvc.perform(post("/rest/profile/load").param("profileData", "JAN,A,0.2|JAN,B,0.18|FEB,A,0.1|FEB,B,0.21"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(false)))
				.andExpect(jsonPath("$.error", is("total fractions of A not ONE\ntotal fractions of B not ONE\n")));
	}
	
	@Test
	public void loadProfilesFail1() throws Exception {
		mockMvc.perform(post("/rest/profile/load").param("profileData1", "JAN,A,0.2|JAN,B,0.18|FEB,A,0.1|FEB,B,0.21"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(false)))
				.andExpect(jsonPath("$.error", is("empty input\n")));
	}

	@Test
	public void loadProfilesSuccess() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/profile/list")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].profileName", is("A"))).andExpect(jsonPath("$[1].profileName", is("B")));

		mockMvc.perform(post("/rest/profiledata/list").param("profileName", "A")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].profile", is("A"))).andExpect(jsonPath("$[0].month", is("JAN")))
				.andExpect(jsonPath("$[0].fraction", is(0.10))).andExpect(jsonPath("$[1].profile", is("A")))
				.andExpect(jsonPath("$[1].month", is("FEB"))).andExpect(jsonPath("$[1].fraction", is(0.10)))
				.andExpect(jsonPath("$[2].profile", is("A"))).andExpect(jsonPath("$[2].month", is("MAR")))
				.andExpect(jsonPath("$[2].fraction", is(0.05))).andExpect(jsonPath("$[3].profile", is("A")))
				.andExpect(jsonPath("$[3].month", is("APR"))).andExpect(jsonPath("$[3].fraction", is(0.05)))
				.andExpect(jsonPath("$[4].profile", is("A"))).andExpect(jsonPath("$[4].month", is("MAY")))
				.andExpect(jsonPath("$[4].fraction", is(0.1))).andExpect(jsonPath("$[5].profile", is("A")))
				.andExpect(jsonPath("$[5].month", is("JUNE"))).andExpect(jsonPath("$[5].fraction", is(0.1)))
				.andExpect(jsonPath("$[6].profile", is("A"))).andExpect(jsonPath("$[6].month", is("JULY")))
				.andExpect(jsonPath("$[6].fraction", is(0.10))).andExpect(jsonPath("$[7].profile", is("A")))
				.andExpect(jsonPath("$[7].month", is("AUG"))).andExpect(jsonPath("$[7].fraction", is(0.10)))
				.andExpect(jsonPath("$[8].profile", is("A"))).andExpect(jsonPath("$[8].month", is("SEPT")))
				.andExpect(jsonPath("$[8].fraction", is(0.10))).andExpect(jsonPath("$[9].profile", is("A")))
				.andExpect(jsonPath("$[9].month", is("OCT"))).andExpect(jsonPath("$[9].fraction", is(0.10)))
				.andExpect(jsonPath("$[10].profile", is("A"))).andExpect(jsonPath("$[10].month", is("NOV")))
				.andExpect(jsonPath("$[10].fraction", is(0.05))).andExpect(jsonPath("$[11].profile", is("A")))
				.andExpect(jsonPath("$[11].month", is("DEC"))).andExpect(jsonPath("$[11].fraction", is(0.05)));

	}

	@Test
	public void deleteProfilesSuccess() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/profile/delete").param("profileName", "A")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

	@Test(expected = NestedServletException.class)
	public void deleteProfilesFail() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/profile/delete").param("profileName", "C"));

	}

	@Test
	public void updateProfilesSuccess() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		MultiValueMap<String, String> multiValue = new LinkedMultiValueMap<String, String>();
		multiValue.add("oldProfileName", "A");
		multiValue.add("newProfileName", "C");

		mockMvc.perform(post("/rest/profile/update").params(multiValue)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

	@Test(expected = NestedServletException.class)
	public void updateProfilesFail() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		MultiValueMap<String, String> multiValue = new LinkedMultiValueMap<String, String>();
		multiValue.add("oldProfileName", "A");
		multiValue.add("newProfileName", "B");

		mockMvc.perform(post("/rest/profile/update").params(multiValue));

	}

	@Test
	public void updateProfilesDataSuccess() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		List<ProfilesDataDTO> profilesDataList = new ArrayList<>();
		profilesDataList.add(new ProfilesDataDTO(Months.JAN, "A", new BigDecimal(0.2).setScale(4, RoundingMode.DOWN)));
		profilesDataList.add(new ProfilesDataDTO(Months.FEB, "A", new BigDecimal(0).setScale(4, RoundingMode.DOWN)));

		mockMvc.perform(post("/rest/profiledata/update").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(profilesDataList))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

	@Test
	public void updateProfilesDataFail() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.05|DEC,B,0.1|SEPT,B,0.025|MAY,B,0.05|JUNE,A,0.1|JAN,B,0.1|JAN,A,0.1|APR,A,0.05|FEB,B,0.2|MAR,B,0.1|FEB,A,0.1|JULY,A,0.1|DEC,A,0.05|MAY,A,0.1|APR,B,0.05|MAR,A,0.05|AUG,A,0.1|OCT,A,0.1|JUNE,B,0.2|JULY,B,0.1|OCT,B,0.025|SEPT,A,0.1|AUG,B,0.025|NOV,B,0.025"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		List<ProfilesDataDTO> profilesDataList = new ArrayList<>();
		profilesDataList.add(new ProfilesDataDTO(Months.JAN, "A", new BigDecimal(0.1).setScale(4, RoundingMode.DOWN)));
		profilesDataList.add(new ProfilesDataDTO(Months.FEB, "A", new BigDecimal(0).setScale(4, RoundingMode.DOWN)));
		profilesDataList.add(new ProfilesDataDTO(Months.MAR, "A", new BigDecimal(0.25).setScale(4, RoundingMode.DOWN)));

		mockMvc.perform(post("/rest/profiledata/update").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(profilesDataList))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(false)));
		;

	}

	@Test
	public void uploadTests() throws Exception {

		mockMvc.perform(post("/rest/profile/uploadfile").param("file",
				"/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/PROFILE")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(false)));

	}
	
	
	@Test
	public void uploadTestsFail() throws Exception {

		mockMvc.perform(post("/rest/profile/uploadfile").param("file1",
				"/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/PROFILE")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(false)));

	}
	
	@Test
	public void uploadTestsSuccess() throws Exception {
	
	    Files.copy(Paths.get("/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/PROFILE_SUCCESS"), Paths.get("/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/PROFILE_SUCCESS_1"), StandardCopyOption.REPLACE_EXISTING);
	    
	    
		mockMvc.perform(post("/rest/profile/uploadfile").param("file",
				"/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/PROFILE_SUCCESS_1")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

	public static String asJsonString(final Object obj) {
		try {
			ObjectMapper om = new ObjectMapper();
			om.setNodeFactory(JsonNodeFactory.withExactBigDecimals(true));
			return om.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
