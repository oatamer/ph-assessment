package com.powerhouse.energy.test;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.powerhouse.energy.Application;
import com.powerhouse.energy.enums.Months;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ReadingRestTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	public void loadData() throws Exception {
		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.1|DEC,B,0.1|SEPT,B,0.1|MAY,B,0.05|JUNE,A,0.05|JAN,B,0.1|JAN,A,0.1|APR,A,0.1|FEB,B,0.1|MAR,B,0.1|FEB,A,0.1|JULY,A,0.05|DEC,A,0.1|MAY,A,0.05|APR,B,0.1|MAR,A,0.1|AUG,A,0.05|OCT,A,0.1|JUNE,B,0.05|JULY,B,0.05|OCT,B,0.1|SEPT,A,0.1|AUG,B,0.05|NOV,B,0.1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/reading/load").param("readingData",
				"0001,A,JAN,5|0001,A,FEB,10|0001,A,MAR,15|0001,A,APR,20|0001,A,MAY,23|0001,A,JUNE,26|0001,A,JULY,29|0001,A,AUG,32|0001,A,SEPT,37|0001,A,OCT,42|0001,A,NOV,47|0001,A,DEC,52|0004,B,JAN,5|0004,B,FEB,10|0004,B,MAR,15|0004,B,APR,20|0004,B,MAY,23|0004,B,JUNE,26|0004,B,JULY,29|0004,B,AUG,32|0004,B,SEPT,37|0004,B,OCT,42|0004,B,NOV,47|0004,B,DEC,52"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

	}

	@Test
	public void loadReadingSuccess() throws Exception {

		loadData();

		mockMvc.perform(post("/rest/reading/list").param("connectionId", "0001")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].connectionId", is("0001"))).andExpect(jsonPath("$[0].month", is("JAN")))
				.andExpect(jsonPath("$[0].reading", is(5))).andExpect(jsonPath("$[0].profile", is("A")))

				.andExpect(jsonPath("$[1].connectionId", is("0001"))).andExpect(jsonPath("$[1].month", is("FEB")))
				.andExpect(jsonPath("$[1].reading", is(10))).andExpect(jsonPath("$[1].profile", is("A")))

				.andExpect(jsonPath("$[2].connectionId", is("0001"))).andExpect(jsonPath("$[2].month", is("MAR")))
				.andExpect(jsonPath("$[2].reading", is(15))).andExpect(jsonPath("$[2].profile", is("A")))

				.andExpect(jsonPath("$[3].connectionId", is("0001"))).andExpect(jsonPath("$[3].month", is("APR")))
				.andExpect(jsonPath("$[3].reading", is(20))).andExpect(jsonPath("$[3].profile", is("A")))

				.andExpect(jsonPath("$[4].connectionId", is("0001"))).andExpect(jsonPath("$[4].month", is("MAY")))
				.andExpect(jsonPath("$[4].reading", is(23))).andExpect(jsonPath("$[4].profile", is("A")))

				.andExpect(jsonPath("$[5].connectionId", is("0001"))).andExpect(jsonPath("$[5].month", is("JUNE")))
				.andExpect(jsonPath("$[5].reading", is(26))).andExpect(jsonPath("$[5].profile", is("A")))

				.andExpect(jsonPath("$[6].connectionId", is("0001"))).andExpect(jsonPath("$[6].month", is("JULY")))
				.andExpect(jsonPath("$[6].reading", is(29))).andExpect(jsonPath("$[6].profile", is("A")))

				.andExpect(jsonPath("$[7].connectionId", is("0001"))).andExpect(jsonPath("$[7].month", is("AUG")))
				.andExpect(jsonPath("$[7].reading", is(32))).andExpect(jsonPath("$[7].profile", is("A")))

				.andExpect(jsonPath("$[8].connectionId", is("0001"))).andExpect(jsonPath("$[8].month", is("SEPT")))
				.andExpect(jsonPath("$[8].reading", is(37))).andExpect(jsonPath("$[8].profile", is("A")))

				.andExpect(jsonPath("$[9].connectionId", is("0001"))).andExpect(jsonPath("$[9].month", is("OCT")))
				.andExpect(jsonPath("$[9].reading", is(42))).andExpect(jsonPath("$[9].profile", is("A")))

				.andExpect(jsonPath("$[10].connectionId", is("0001"))).andExpect(jsonPath("$[10].month", is("NOV")))
				.andExpect(jsonPath("$[10].reading", is(47))).andExpect(jsonPath("$[10].profile", is("A")))

				.andExpect(jsonPath("$[11].connectionId", is("0001"))).andExpect(jsonPath("$[11].month", is("DEC")))
				.andExpect(jsonPath("$[11].reading", is(52))).andExpect(jsonPath("$[11].profile", is("A")));

	}

	@Test
	public void loadReadingFail() throws Exception {

		mockMvc.perform(post("/rest/reading/load").param("readingData1",
				"0001,A,JAN,5|0001,A,FEB,10|0001,A,MAR,15|0001,A,APR,20|0001,A,MAY,23|0001,A,JUNE,26|0001,A,JULY,29|0001,A,AUG,32|0001,A,SEPT,37|0001,A,OCT,42|0001,A,NOV,47|0001,A,DEC,52|0004,B,JAN,5|0004,B,FEB,10|0004,B,MAR,15|0004,B,APR,20|0004,B,MAY,23|0004,B,JUNE,26|0004,B,JULY,29|0004,B,AUG,32|0004,B,SEPT,37|0004,B,OCT,42|0004,B,NOV,47|0004,B,DEC,52"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(false)));
	}
	
	
	
	@Test
	public void loadDataFail1() throws Exception {
		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.1|DEC,B,0.1|SEPT,B,0.1|MAY,B,0.05|JUNE,A,0.05|JAN,B,0.1|JAN,A,0.1|APR,A,0.1|FEB,B,0.1|MAR,B,0.1|FEB,A,0.1|JULY,A,0.05|DEC,A,0.1|MAY,A,0.05|APR,B,0.1|MAR,A,0.1|AUG,A,0.05|OCT,A,0.1|JUNE,B,0.05|JULY,B,0.05|OCT,B,0.1|SEPT,A,0.1|AUG,B,0.05|NOV,B,0.1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/reading/load").param("readingData",
				"0001,C,JAN,5|0001,A,FEB,10|0001,A,MAR,15|0001,A,APR,20|0001,A,MAY,23|0001,A,JUNE,26|0001,A,JULY,29|0001,A,AUG,32|0001,A,SEPT,37|0001,A,OCT,42|0001,A,NOV,47|0001,A,DEC,52|0004,B,JAN,5|0004,B,FEB,10|0004,B,MAR,15|0004,B,APR,20|0004,B,MAY,23|0004,B,JUNE,26|0004,B,JULY,29|0004,B,AUG,32|0004,B,SEPT,37|0004,B,OCT,42|0004,B,NOV,47|0004,B,DEC,52"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(false)));

	}
	
	@Test
	public void loadDataFail2() throws Exception {
		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.1|DEC,B,0.1|SEPT,B,0.1|MAY,B,0.05|JUNE,A,0.05|JAN,B,0.1|JAN,A,0.1|APR,A,0.1|FEB,B,0.1|MAR,B,0.1|FEB,A,0.1|JULY,A,0.05|DEC,A,0.1|MAY,A,0.05|APR,B,0.1|MAR,A,0.1|AUG,A,0.05|OCT,A,0.1|JUNE,B,0.05|JULY,B,0.05|OCT,B,0.1|SEPT,A,0.1|AUG,B,0.05|NOV,B,0.1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/reading/load").param("readingData",
				"0001,A,JAN,5|0001,A,FEB,4|0001,A,MAR,15|0001,A,APR,20|0001,A,MAY,23|0001,A,JUNE,26|0001,A,JULY,29|0001,A,AUG,32|0001,A,SEPT,37|0001,A,OCT,42|0001,A,NOV,47|0001,A,DEC,52|0004,B,JAN,5|0004,B,FEB,10|0004,B,MAR,15|0004,B,APR,20|0004,B,MAY,23|0004,B,JUNE,26|0004,B,JULY,29|0004,B,AUG,32|0004,B,SEPT,37|0004,B,OCT,42|0004,B,NOV,47|0004,B,DEC,52"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(false)));

	}

	@Test
	public void deleteReadingSuccess() throws Exception {

		loadData();

		mockMvc.perform(post("/rest/reading/delete").param("connectionId", "0004")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

	@Test
	public void updateReading() throws Exception {
		loadData();

		MultiValueMap<String, String> multiValue = new LinkedMultiValueMap<String, String>();
		multiValue.add("connectionId", "0001");
		multiValue.add("month", Months.JAN.name());
		multiValue.add("newReading", "6");

		mockMvc.perform(post("/rest/reading/update").params(multiValue)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

	}

	@Test
	public void retreiveConsumption() throws Exception {
		loadData();

		mockMvc.perform(post("/rest/reading/retreiveconsumption").param("month", Months.FEB.name()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(10)));

	}

	@Test
	public void uploadTests() throws Exception {

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.1|DEC,B,0.1|SEPT,B,0.1|MAY,B,0.05|JUNE,A,0.05|JAN,B,0.1|JAN,A,0.1|APR,A,0.1|FEB,B,0.1|MAR,B,0.1|FEB,A,0.1|JULY,A,0.05|DEC,A,0.1|MAY,A,0.05|APR,B,0.1|MAR,A,0.1|AUG,A,0.05|OCT,A,0.1|JUNE,B,0.05|JULY,B,0.05|OCT,B,0.1|SEPT,A,0.1|AUG,B,0.05|NOV,B,0.1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/reading/uploadfile").param("file",
				"/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/READING")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(false)));

	}

	@Test
	public void uploadTestsSuccess() throws Exception {

		Files.copy(Paths.get("/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/READING_SUCCESS"),
				Paths.get("/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/READING_SUCCESS_1"),
				StandardCopyOption.REPLACE_EXISTING);

		mockMvc.perform(post("/rest/profile/load").param("profileData",
				"NOV,A,0.1|DEC,B,0.1|SEPT,B,0.1|MAY,B,0.05|JUNE,A,0.05|JAN,B,0.1|JAN,A,0.1|APR,A,0.1|FEB,B,0.1|MAR,B,0.1|FEB,A,0.1|JULY,A,0.05|DEC,A,0.1|MAY,A,0.05|APR,B,0.1|MAR,A,0.1|AUG,A,0.05|OCT,A,0.1|JUNE,B,0.05|JULY,B,0.05|OCT,B,0.1|SEPT,A,0.1|AUG,B,0.05|NOV,B,0.1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.success", is(true)));

		mockMvc.perform(post("/rest/reading/uploadfile").param("file",
				"/Users/Macintosh/Desktop/assesment/powerhouse/src/main/resources/READING_SUCCESS_1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", is(true)));

	}

}
