package com.powerhouse.energy.rest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.powerhouse.energy.dto.ProfilesDTO;
import com.powerhouse.energy.dto.ProfilesDataDTO;
import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.service.IProfileService;
import com.powerhouse.energy.validator.ValidationResult;

@RestController
@RequestMapping("/rest")
public class ProfileRestService {

	@Autowired
	private IProfileService<ProfilesDataDTO> profileService;

	private static final Logger logger = Logger.getLogger(ProfileRestService.class);

	/*
	 * this rest service takes data as Month,Profile,Fraction and multiple
	 * records separated by | character
	 */
	@RequestMapping(value = "/profile/load", method = RequestMethod.POST)
	public ValidationResult loadProfilesData(String profileData) {

		ValidationResult result;
		if (profileData != null) {
			List<ProfilesDataDTO> profilesDTOList = new ArrayList<>();
			Stream<String> stream = Arrays.stream(profileData.split("\\|")).map(s -> s.split(","))
					.flatMap(Arrays::stream);
			Iterator<String> iterStream = stream.iterator();
			while (iterStream.hasNext()) {
				ProfilesDataDTO profileDTO = new ProfilesDataDTO(Months.valueOf(iterStream.next()), iterStream.next(),
						new BigDecimal(iterStream.next()));
				profilesDTOList.add(profileDTO);
			}
			result = profileService.validateLoadData(profilesDTOList);
		} else {
			result = new ValidationResult();
			result.setSuccess(false);
			result.addError("empty input");
		}
		return result;
	}

	@RequestMapping(value = "/profile/list", method = RequestMethod.POST)
	public List<ProfilesDTO> listProfiles() {
		return profileService.listProfiles();
	}

	@RequestMapping(value = "/profile/delete", method = RequestMethod.POST)
	public boolean deleteProfile(String profileName) throws DataException {
		return profileService.deleteProfile(profileName);
	}

	@RequestMapping(value = "/profile/update", method = RequestMethod.POST)
	public boolean updateProfile(String oldProfileName, String newProfileName) throws DataException {
		return profileService.updateProfile(oldProfileName, newProfileName);
	}

	@RequestMapping(value = "/profiledata/list", method = RequestMethod.POST)
	public List<ProfilesDataDTO> listProfilesData(String profileName) {
		return profileService.listProfilesData(profileName);
	}

	@RequestMapping(value = "/profiledata/update", method = RequestMethod.POST)
	public boolean updateProfilesData(@RequestBody List<ProfilesDataDTO> profileDataList) throws DataException {
		return profileService.updateProfilesData(profileDataList);
	}

	@RequestMapping(value = "/profile/uploadfile", method = RequestMethod.POST)
	public boolean uploadFile(String file) throws IOException {
		ValidationResult result;
		if (file != null) {
			List<ProfilesDataDTO> profilesDTOList = new ArrayList<>();
			Stream<String> stream = Arrays.stream(new String(Files.readAllBytes(Paths.get(file))).split("\n"))
					.map(s -> s.split(",")).flatMap(Arrays::stream);
			Iterator<String> iterStream = stream.iterator();
			while (iterStream.hasNext()) {
				ProfilesDataDTO profileDTO = new ProfilesDataDTO(Months.valueOf(iterStream.next()), iterStream.next(),
						new BigDecimal(iterStream.next()));
				profilesDTOList.add(profileDTO);
			}
			result = profileService.validateLoadData(profilesDTOList);
		} else {
			result = new ValidationResult();
			result.setSuccess(false);
			result.addError("empty input");
		}
		if (result.isSuccess()) {
			new File(file).delete();
		} else {
			Files.write(Paths.get(file + "_log"), result.getError().getBytes());
		}

		return result.isSuccess();
	}
}
