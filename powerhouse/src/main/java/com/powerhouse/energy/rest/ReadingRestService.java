package com.powerhouse.energy.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.service.IReadingService;
import com.powerhouse.energy.validator.ValidationResult;

@RestController
@RequestMapping("/rest/reading")
public class ReadingRestService {

	@Autowired
	private IReadingService<ReadingsDTO> readingService;

	/*
	 * this rest service takes data as ConnectionId,Profile,Month ,Meter reading
	 * and multiple records separated by | character
	 */
	@RequestMapping(value = "/load", method = RequestMethod.POST)
	public ValidationResult loadReadingsData(String readingData) {
		ValidationResult result;
		if (readingData != null) {
			List<ReadingsDTO> readingsDTOList = new ArrayList<>();
			Stream<String> stream = Arrays.stream(readingData.split("\\|")).map(s -> s.split(","))
					.flatMap(Arrays::stream);
			Iterator<String> iterStream = stream.iterator();
			while (iterStream.hasNext()) {
				ReadingsDTO readingDTO = new ReadingsDTO(iterStream.next(), iterStream.next(),
						Months.valueOf(iterStream.next()), Integer.valueOf(iterStream.next()));
				readingsDTOList.add(readingDTO);
			}
			result = readingService.validateLoadData(readingsDTOList);
		} else {
			result = new ValidationResult();
			result.setSuccess(false);
			result.addError("empty input");
		}
		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public List<ReadingsDTO> listReadings(String connectionId) {
		return readingService.listReadingsData(connectionId);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public boolean deleteReading(String connectionId) throws DataException {
		return readingService.deleteReadings(connectionId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ValidationResult updateReading(String connectionId, Months month, Integer newReading) throws DataException {
		return readingService.updateReadings(connectionId, month, newReading);
	}

	@RequestMapping(value = "/retreiveconsumption", method = RequestMethod.POST)
	public Integer retreiveConsumption(Months month) throws DataException {
		return readingService.retreiveConsumption(month);
	}

	@RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
	public boolean uploadFile(String file) throws IOException {
		ValidationResult result;
		if (file != null) {
			List<ReadingsDTO> readingsDTOList = new ArrayList<>();
			Stream<String> stream = Arrays.stream(new String(Files.readAllBytes(Paths.get(file))).split("\n"))
					.map(s -> s.split(",")).flatMap(Arrays::stream);
			Iterator<String> iterStream = stream.iterator();
			while (iterStream.hasNext()) {
				ReadingsDTO readingDTO = new ReadingsDTO(iterStream.next(), iterStream.next(),
						Months.valueOf(iterStream.next()), Integer.valueOf(iterStream.next()));
				readingsDTOList.add(readingDTO);
			}
			result = readingService.validateLoadData(readingsDTOList);
		} else {
			result = new ValidationResult();
			result.setSuccess(false);
			result.addError("empty input");
		}

		if (result.isSuccess()) {
			new File(file).delete();
		} else {
			Files.write(Paths.get(file + "_log"), result.getError().getBytes());
		}

		return result.isSuccess();
	}

}
