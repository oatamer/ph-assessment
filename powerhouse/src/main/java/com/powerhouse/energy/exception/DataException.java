package com.powerhouse.energy.exception;

public class DataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataException(String exception) {
		super(exception);
	}

}
