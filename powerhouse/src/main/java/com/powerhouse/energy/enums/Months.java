package com.powerhouse.energy.enums;

public enum Months {
	JAN, FEB, MAR, APR, MAY, JUNE, JULY, AUG, SEPT, OCT, NOV, DEC;

	public static Months findFromOrdinal(int ordinal) {
		for (Months m : Months.values()) {
			if (m.ordinal() == ordinal) {
				return m;
			}
		}
		return null;
	}
}
