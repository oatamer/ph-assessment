package com.powerhouse.energy.service;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.powerhouse.energy.dto.ProfilesDTO;
import com.powerhouse.energy.dto.ProfilesDataDTO;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.model.Profiles;
import com.powerhouse.energy.model.ProfilesData;
import com.powerhouse.energy.repository.ProfileRepository;
import com.powerhouse.energy.repository.ProfilesDataRepository;
import com.powerhouse.energy.validator.IValidator;
import com.powerhouse.energy.validator.ValidationResult;

@Service
public class ProfileServiceImpl implements IProfileService<ProfilesDataDTO> {

	@Autowired
	@Qualifier("profileValidator")
	private IValidator<ProfilesDataDTO> profilesValidator;

	@Autowired
	private ProfilesDataRepository profilesDataRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public List<ProfilesDTO> listProfiles() {
		return profileRepository.findAll().stream().map(p -> new ProfilesDTO(p.getProfileName()))
				.collect(Collectors.toList());
	}

	@Override
	public ValidationResult validateLoadData(List<ProfilesDataDTO> profileDTOList) {
		ValidationResult resultAll = ValidationResult.success();

		profileDTOList.stream().collect(groupingBy(ProfilesDataDTO::getProfile)).values().forEach(list -> {
			ValidationResult result = profilesValidator.validate(list);
			if (result.isSuccess()) {
				// add reading records
				loadData(list);
			} else {
				resultAll.addError(result.getError());
			}
			resultAll.setSuccess(resultAll.isSuccess() && result.isSuccess());
		});
		return resultAll;
	}

	private void loadData(List<ProfilesDataDTO> list) {
		// check if profile exist before
		String profileName = list.get(0).getProfile();
		Profiles profile = profileRepository.findByProfileName(profileName);
		if (profile == null) {
			final Profiles profilePersist = profileRepository.save(new Profiles(profileName));
			list.sort((ProfilesDataDTO p1, ProfilesDataDTO p2) -> p1.getMonth().compareTo(p2.getMonth()));
			list.forEach(
					p -> profilesDataRepository.save(new ProfilesData(p.getMonth(), profilePersist, p.getFraction())));
		}
	}

	@Override
	public List<ProfilesDataDTO> listProfilesData(String profileName) {
		Profiles profile = profileRepository.findByProfileName(profileName);
		if (profile != null) {
			return profilesDataRepository.findByProfile(profile).stream()
					.map(p -> new ProfilesDataDTO(p.getMonth(), profile.getProfileName(), p.getFraction()))
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Override
	@Transactional(value = TxType.REQUIRES_NEW)
	public boolean deleteProfile(String profileName) throws DataException {
		Profiles profile = profileRepository.findByProfileName(profileName);
		if (profile != null) {
			profilesDataRepository.deleteByProfile(profile);
			profileRepository.delete(profile);
			return true;
		} else {
			throw new DataException("profile can not be found");
		}

	}

	@Override
	public boolean updateProfile(String oldProfileName, String newProfileName) throws DataException {
		Profiles oldProfile = profileRepository.findByProfileName(oldProfileName);
		if (oldProfile != null) {
			Profiles newProfile = profileRepository.findByProfileName(newProfileName);
			if (newProfile == null) {
				oldProfile.setProfileName(newProfileName);
				profileRepository.save(oldProfile);
				return true;
			} else {
				throw new DataException("profile already exist");
			}
		} else {
			throw new DataException("profile can not be found");
		}
	}

	@Override
	public boolean updateProfilesData(List<ProfilesDataDTO> profileDataList) throws DataException {
		ProfilesDataDTO profileDataDTO = profileDataList.get(0);
		Profiles profile = profileRepository.findByProfileName(profileDataDTO.getProfile());
		if (profile != null) {
			List<ProfilesDataDTO> list = profilesDataRepository.findByProfile(profile).stream()
					.map(p -> new ProfilesDataDTO(p.getId(), p.getMonth(), profile.getProfileName(), p.getFraction()))
					.collect(Collectors.toList());

			profileDataList.forEach(pd -> list.stream().filter(p -> p.getMonth().equals(pd.getMonth())).findFirst()
					.ifPresent(foundProfileData -> {
						foundProfileData.setFraction(pd.getFraction());
						foundProfileData.setDirty(true);
					}));

			ValidationResult result = profilesValidator.validate(list);
			if (result.isSuccess()) {
				// add reading records
				return updateData(list, profile);
			} else {
				return false;
			}
		} else {
			throw new DataException("profile can not be found");
		}
	}

	private boolean updateData(List<ProfilesDataDTO> list, Profiles profile) {
		list.stream().filter(p -> p.isDirty())
				.map(pm -> new ProfilesData(pm.getId(), pm.getMonth(), profile, pm.getFraction()))
				.forEach(pe -> profilesDataRepository.save(pe));
		return true;
	}

}
