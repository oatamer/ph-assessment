package com.powerhouse.energy.service;

import java.util.List;

import com.powerhouse.energy.dto.BaseDTO;
import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.validator.ValidationResult;

public interface IReadingService<T extends BaseDTO> {
	ValidationResult validateLoadData(List<T> readingDTOList);

	List<ReadingsDTO> listReadingsData(String connectionId);

	ValidationResult updateReadings(String connectionId, Months month, Integer newReading) throws DataException;

	boolean deleteReadings(String connectionId) throws DataException;

	Integer retreiveConsumption(Months month);


}
