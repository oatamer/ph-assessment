package com.powerhouse.energy.service;

import static java.util.stream.Collectors.groupingBy;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.model.Profiles;
import com.powerhouse.energy.model.Readings;
import com.powerhouse.energy.repository.ProfileRepository;
import com.powerhouse.energy.repository.ReadingsRepository;
import com.powerhouse.energy.validator.IValidator;
import com.powerhouse.energy.validator.ValidationResult;

@Service
public class ReadingServiceImpl implements IReadingService<ReadingsDTO> {

	@Autowired
	@Qualifier("readingValidator")
	private IValidator<ReadingsDTO> readingValidator;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ReadingsRepository readingRepository;

	@Override
	public ValidationResult validateLoadData(List<ReadingsDTO> readingDTOList) {
		ValidationResult resultAll = ValidationResult.success();

		readingDTOList.stream().collect(groupingBy(ReadingsDTO::getConnectionId)).values().forEach(list -> {
			// sort list before validate
			list.sort((r1, r2) -> r1.getMonth().compareTo(r2.getMonth()));
			ValidationResult result = readingValidator.validate(list);
			if (result.isSuccess()) {
				// add reading records
				loadData(list);
			} else {
				resultAll.addError(result.getError());
			}
			resultAll.setSuccess(resultAll.isSuccess() && result.isSuccess());
		});
		return resultAll;
	}

	private void loadData(List<ReadingsDTO> list) {

		String profileName = list.get(0).getProfile();
		Profiles profile = profileRepository.findByProfileName(profileName);
		if (profile != null) {
			list.forEach(p -> readingRepository
					.save(new Readings(p.getConnectionId(), profile, p.getMonth(), p.getReading())));
		}

	}

	@Override
	public List<ReadingsDTO> listReadingsData(String connectionId) {
		return readingRepository.findByConnectionId(connectionId).stream().map(p -> new ReadingsDTO(p.getConnectionId(),
				p.getProfile().getProfileName(), p.getMonth(), p.getReading())).collect(Collectors.toList());

	}

	@Override
	@Transactional
	public boolean deleteReadings(String connectionId) throws DataException {
		readingRepository.deleteByConnectionId(connectionId);
		return true;
	}

	@Override
	public ValidationResult updateReadings(String connectionId, Months month, Integer newReading) throws DataException {

		ValidationResult result;
		List<Readings> readingsDataList = readingRepository.findByConnectionId(connectionId);
		if (!readingsDataList.isEmpty()) {
			List<ReadingsDTO> readingsDTOList = readingsDataList.stream().map(p -> new ReadingsDTO(p.getId(),
					p.getConnectionId(), p.getProfile().getProfileName(), p.getMonth(), p.getReading()))
					.collect(Collectors.toList());

			readingsDTOList.stream().filter(p -> p.getMonth().equals(month)).findFirst().ifPresent(p -> {
				p.setReading(newReading);
				p.setDirty(true);
			});
			result = readingValidator.validate(readingsDTOList);
			if (result.isSuccess()) {
				// update reading records
				updateData(readingsDTOList, readingsDataList.get(0).getProfile());
			}
		} else {
			result = new ValidationResult();
			result.setSuccess(false);
			result.setError("connection not found " + connectionId);
		}
		return result;

	}

	private boolean updateData(List<ReadingsDTO> readingsDTOList, Profiles profile) {
		readingsDTOList.stream().filter(p -> p.isDirty())
				.map(pm -> new Readings(pm.getId(), pm.getConnectionId(), profile, pm.getMonth(), pm.getReading()))
				.forEach(pe -> readingRepository.save(pe));
		return true;

	}

	@Override
	public Integer retreiveConsumption(Months month) {
		int ordinal = month.ordinal();
		if (ordinal == 0) {
			List<Readings> readingsList = readingRepository.findByMonth(month);
			return readingsList.stream().map(r -> r.getReading()).reduce(0, Integer::sum);
		} else {
			Months previous = Months.findFromOrdinal(ordinal - 1);
			List<Readings> readingsList = readingRepository.findFromTwoMonths(month, previous);
			return readingsList.stream().collect(groupingBy(Readings::getConnectionId)).values().stream()
					.map(l -> Math.abs(l.get(0).getReading() - l.get(1).getReading())).reduce(0, Integer::sum);
		}
	}



}
