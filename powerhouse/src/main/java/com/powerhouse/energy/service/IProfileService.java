package com.powerhouse.energy.service;

import java.util.List;

import com.powerhouse.energy.dto.BaseDTO;
import com.powerhouse.energy.dto.ProfilesDTO;
import com.powerhouse.energy.dto.ProfilesDataDTO;
import com.powerhouse.energy.exception.DataException;
import com.powerhouse.energy.validator.ValidationResult;

public interface IProfileService<T extends BaseDTO> {
	ValidationResult validateLoadData(List<T> profileDTOList);

	List<ProfilesDTO> listProfiles();

	List<ProfilesDataDTO> listProfilesData(String profileName);

	boolean deleteProfile(String profileName) throws DataException;

	boolean updateProfile(String oldProfileName, String newProfileName) throws DataException;

	boolean updateProfilesData(List<ProfilesDataDTO> profileDataList) throws DataException;
}
