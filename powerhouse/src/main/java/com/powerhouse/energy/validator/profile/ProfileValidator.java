package com.powerhouse.energy.validator.profile;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.powerhouse.energy.dto.ProfilesDataDTO;
import com.powerhouse.energy.validator.IValidator;
import com.powerhouse.energy.validator.ValidationResult;

@Service
public class ProfileValidator implements IValidator<ProfilesDataDTO> {

	@Override
	public ValidationResult validate(List<ProfilesDataDTO> dto) {

		ValidationResult validation;
		BigDecimal sum = dto.stream().map(d -> d.getFraction()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (sum.compareTo(BigDecimal.ONE) == 0) {
			validation = ValidationResult.success();
		} else {
			validation = new ValidationResult();
			validation.setError("total fractions of " + dto.get(0).getProfile() + " not ONE");
		}
		return validation;
	}
}
