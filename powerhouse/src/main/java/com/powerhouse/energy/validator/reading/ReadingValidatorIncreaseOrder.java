package com.powerhouse.energy.validator.reading;

import java.util.List;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.validator.ValidationResult;

@Component
@Order(value = 2)
public class ReadingValidatorIncreaseOrder implements IValidatorReading {

	@Override
	public ValidationResult validate(List<ReadingsDTO> dto) {

		ValidationResult validation = ValidationResult.success();
		Integer next = 0;
		for (ReadingsDTO reading : dto) {
			if (reading.getReading() < next) {
				validation.setSuccess(false);
				validation.addError(reading.getMonth() + " month must be higher than previous month");
				break;
			} else {
				next = reading.getReading();
			}
		}
		return validation;
	}

}
