package com.powerhouse.energy.validator.reading;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.model.Profiles;
import com.powerhouse.energy.model.ProfilesData;
import com.powerhouse.energy.repository.ProfileRepository;
import com.powerhouse.energy.repository.ProfilesDataRepository;
import com.powerhouse.energy.validator.ValidationResult;

@Component
@Order(value = 3)
public class ReadingValidatorProfilePercent implements IValidatorReading {

	@Autowired
	private ProfilesDataRepository profilesDataRepository;

	@Autowired
	private ProfileRepository profilesRepository;

	@Override
	public ValidationResult validate(List<ReadingsDTO> dto) {

		ValidationResult validation = ValidationResult.success();

		Profiles profile = profilesRepository.findByProfileName(dto.get(0).getProfile());
		if (profile != null) {
			List<ProfilesData> profileDataList = profilesDataRepository.findByProfile(profile);
			BigDecimal totalConsumption = BigDecimal.valueOf(dto.get(dto.size() - 1).getReading());
			BigDecimal lastReading = BigDecimal.ZERO;
			for (int i = 0; i < dto.size(); i++) {
				ProfilesData profileData = profileDataList.get(i);
				ReadingsDTO readingsDTO = dto.get(i);

				BigDecimal nextReading = BigDecimal.valueOf(readingsDTO.getReading());
				BigDecimal monthReading = nextReading.subtract(lastReading);
				BigDecimal minVal = profileData.getFraction().multiply(BigDecimal.valueOf(0.75))
						.multiply(totalConsumption);
				BigDecimal maxVal = profileData.getFraction().multiply(BigDecimal.valueOf(1.25))
						.multiply(totalConsumption);
				if (monthReading.compareTo(minVal) == -1 || monthReading.compareTo(maxVal) == 1) {
					validation.setSuccess(false);
					validation.setError("reading value for month " + readingsDTO.getMonth() + " is invalid");
					break;
				}
				lastReading = nextReading;
			}
		} else {
			validation.setSuccess(false);
			validation.setError("profile  not found");
		}

		return validation;
	}

}
