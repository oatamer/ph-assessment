package com.powerhouse.energy.validator.reading;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.validator.IValidator;
import com.powerhouse.energy.validator.ValidationResult;

@Service
public class ReadingValidator implements IValidator<ReadingsDTO> {

	@Autowired
	private List<IValidatorReading> validatorList;

	@Override
	public ValidationResult validate(List<ReadingsDTO> dto) {
		ValidationResult resultTotal = ValidationResult.success();
		for (IValidatorReading validator : validatorList) {
			ValidationResult result = validator.validate(dto);
			resultTotal.addError(result.getError());
			resultTotal.setSuccess(result.isSuccess());
			if (!result.isSuccess()) {
				break;
			}
		}
		return resultTotal;
	}

}
