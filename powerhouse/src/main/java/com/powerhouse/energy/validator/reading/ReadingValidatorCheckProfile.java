package com.powerhouse.energy.validator.reading;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.model.Profiles;
import com.powerhouse.energy.repository.ProfileRepository;
import com.powerhouse.energy.validator.ValidationResult;

@Component
@Order(value = 1)
public class ReadingValidatorCheckProfile implements IValidatorReading {

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public ValidationResult validate(List<ReadingsDTO> dto) {
		ValidationResult validation = ValidationResult.success();

		List<String> profiles = dto.stream().map(r -> r.getProfile()).distinct().collect(Collectors.toList());
		if (profiles.size() == 1) {
			String profileName = profiles.get(0);
			Profiles foundProfile = profileRepository.findByProfileName(profiles.get(0));
			if (foundProfile == null) {
				validation.setSuccess(false);
				validation.setError("profile " + profileName + " not found");
			}
		} else {
			validation.setSuccess(false);
			validation.setError("profile data unconsistent");
		}
		return validation;
	}

}
