package com.powerhouse.energy.validator;

import java.util.List;

import com.powerhouse.energy.dto.BaseDTO;

public interface IValidator<T extends BaseDTO> {

	public ValidationResult validate(List<T> dto);
}
