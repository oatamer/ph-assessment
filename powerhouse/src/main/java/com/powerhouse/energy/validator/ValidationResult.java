package com.powerhouse.energy.validator;

public class ValidationResult {

	private boolean success;
	private String error = "";

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void addError(String error) {
		this.error = this.error + error + "\n";
	}

	public static ValidationResult success() {
		ValidationResult result = new ValidationResult();
		result.success = true;
		return result;
	}

}
