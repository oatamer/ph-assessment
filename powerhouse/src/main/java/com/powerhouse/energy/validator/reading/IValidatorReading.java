package com.powerhouse.energy.validator.reading;

import java.util.List;

import com.powerhouse.energy.dto.ReadingsDTO;
import com.powerhouse.energy.validator.ValidationResult;

public interface IValidatorReading {

	ValidationResult validate(List<ReadingsDTO> dto);

}
