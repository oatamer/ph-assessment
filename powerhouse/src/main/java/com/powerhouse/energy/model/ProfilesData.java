package com.powerhouse.energy.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.powerhouse.energy.enums.Months;

@Entity
public class ProfilesData {
	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Months month;

	@ManyToOne(optional = false)
	@JoinColumn(name = "profile_id")
	private Profiles profile;

	@Column(nullable = false)
	private BigDecimal fraction;

	public ProfilesData(Months month, Profiles profile, BigDecimal fraction) {
		this.month = month;
		this.profile = profile;
		this.fraction = fraction;
	}

	public ProfilesData(Long id, Months month, Profiles profile, BigDecimal fraction) {
		this.id = id;
		this.month = month;
		this.profile = profile;
		this.fraction = fraction;
	}

	// default constructor
	public ProfilesData() {

	}

	public Long getId() {
		return id;
	}

	public Months getMonth() {
		return month;
	}

	public void setMonth(Months month) {
		this.month = month;
	}

	public Profiles getProfile() {
		return profile;
	}

	public void setProfile(Profiles profile) {
		this.profile = profile;
	}

	public BigDecimal getFraction() {
		return fraction;
	}

	public void setFraction(BigDecimal fraction) {
		this.fraction = fraction;
	}

}
