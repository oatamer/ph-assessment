package com.powerhouse.energy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.powerhouse.energy.enums.Months;

@Entity
public class Readings {
	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String connectionId;

	@ManyToOne(optional = false)
	@JoinColumn(name = "profile_id")
	private Profiles profile;

	@Enumerated(EnumType.STRING)
	private Months month;

	@Column(nullable = false)
	private Integer reading;

	public Readings(String connectionId, Profiles profile, Months month, Integer reading) {
		this.connectionId = connectionId;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}

	public Readings(Long id, String connectionId, Profiles profile, Months month, Integer reading) {
		this.id = id;
		this.connectionId = connectionId;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}

	public Readings() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public Profiles getProfile() {
		return profile;
	}

	public void setProfile(Profiles profile) {
		this.profile = profile;
	}

	public Months getMonth() {
		return month;
	}

	public void setMonth(Months month) {
		this.month = month;
	}

	public Integer getReading() {
		return reading;
	}

	public void setReading(Integer reading) {
		this.reading = reading;
	}

}
