package com.powerhouse.energy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Profiles {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String profileName;

	// default constructor
	public Profiles() {

	}

	public Profiles(String profileName) {
		this.profileName = profileName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

}
