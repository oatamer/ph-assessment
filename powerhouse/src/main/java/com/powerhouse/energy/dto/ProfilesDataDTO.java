package com.powerhouse.energy.dto;

import java.math.BigDecimal;

import com.powerhouse.energy.enums.Months;

public class ProfilesDataDTO extends BaseDTO {

	private Months month;

	private String profile;

	private BigDecimal fraction;

	private boolean dirty;

	private Long id;

	public ProfilesDataDTO() {

	}

	public ProfilesDataDTO(Months month, String profile, BigDecimal fraction) {
		this.month = month;
		this.profile = profile;
		this.fraction = fraction;
	}

	public ProfilesDataDTO(Long id, Months month, String profile, BigDecimal fraction) {
		this.id = id;
		this.month = month;
		this.profile = profile;
		this.fraction = fraction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public Months getMonth() {
		return month;
	}

	public void setMonth(Months month) {
		this.month = month;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public BigDecimal getFraction() {
		return fraction;
	}

	public void setFraction(BigDecimal fraction) {
		this.fraction = fraction;
	}

}
