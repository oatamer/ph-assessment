package com.powerhouse.energy.dto;

public class ProfilesDTO extends BaseDTO {

	private String profileName;

	public ProfilesDTO(String profileName) {
		this.profileName = profileName;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

}
