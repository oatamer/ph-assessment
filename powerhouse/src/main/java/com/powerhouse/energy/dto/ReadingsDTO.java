package com.powerhouse.energy.dto;

import com.powerhouse.energy.enums.Months;

public class ReadingsDTO extends BaseDTO {

	private Long id;
	private String connectionId;

	private String profile;

	private Months month;

	private Integer reading;

	private boolean dirty;

	public ReadingsDTO(String connectionId, String profile, Months month, Integer reading) {
		this.connectionId = connectionId;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}

	public ReadingsDTO(Long id, String connectionId, String profile, Months month, Integer reading) {
		this.id = id;
		this.connectionId = connectionId;
		this.profile = profile;
		this.month = month;
		this.reading = reading;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Months getMonth() {
		return month;
	}

	public void setMonth(Months month) {
		this.month = month;
	}

	public Integer getReading() {
		return reading;
	}

	public void setReading(Integer reading) {
		this.reading = reading;
	}

}
