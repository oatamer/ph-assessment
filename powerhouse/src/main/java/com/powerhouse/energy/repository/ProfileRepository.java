package com.powerhouse.energy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.powerhouse.energy.model.Profiles;

public interface ProfileRepository extends JpaRepository<Profiles, Long> {

	Profiles findByProfileName(String profileName);
}
