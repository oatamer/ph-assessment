package com.powerhouse.energy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.powerhouse.energy.model.Profiles;
import com.powerhouse.energy.model.ProfilesData;

public interface ProfilesDataRepository extends JpaRepository<ProfilesData, Long> {

	List<ProfilesData> findByProfile(Profiles profile);

	void deleteByProfile(Profiles profile);
}
