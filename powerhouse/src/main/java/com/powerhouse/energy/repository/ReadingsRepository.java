package com.powerhouse.energy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.powerhouse.energy.enums.Months;
import com.powerhouse.energy.model.Readings;

public interface ReadingsRepository extends JpaRepository<Readings, Long> {

	List<Readings> findByConnectionId(String connectionId);

	void deleteByConnectionId(String connectionId);

	List<Readings> findByMonth(Months month);

	@Query("SELECT p FROM Readings p WHERE p.month = :month OR p.month = :previous")
	List<Readings> findFromTwoMonths(@Param("month") Months month, @Param("previous") Months previous);

}
